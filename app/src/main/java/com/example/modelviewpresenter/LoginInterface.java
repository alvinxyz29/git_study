package com.example.modelviewpresenter;

public interface LoginInterface {
    interface Presenter{
        void attachView(View view);
        boolean validationLogin(String userName,String password);
        void doLogin(String userName,String password);
    }

    interface View{
        void onError(String Message);
        void onSuccess(String Url);
    }
}
