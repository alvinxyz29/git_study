package com.example.modelviewpresenter;

public class LoginPresenter implements LoginInterface.Presenter {
    LoginInterface.View view;

    @Override
    public void attachView(LoginInterface.View view) {

        this.view = view;
    }

    @Override
    public boolean validationLogin(String userName, String password) {
        if (userName.isEmpty()||userName.contains("!")){
            view.onError("Username tidak boleh kosong!");
            return false;
        }
        if (password.isEmpty()) {
            view.onError("Password tidak boleh kosong!");
            return false;
        }

        return true;
    }

    @Override
    public void doLogin(String userName, String password) {
        if (userName.equalsIgnoreCase("tester")&&password.equalsIgnoreCase("password")){
            view.onSuccess("http://www.facebook.com");

        }else{
            view.onError("Password dan Username tidak cocok");
        }
    }
}
