package com.example.modelviewpresenter;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements LoginInterface.View {

    LoginInterface.Presenter presenter;
    EditText etUsername;
    EditText etPassword;
    Button btnLogiin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        etUsername = findViewById(R.id.etUsername);
        etPassword = findViewById(R.id.etPassword);
        btnLogiin = findViewById(R.id.btnLogin);


        presenter = new LoginPresenter();
        presenter.attachView(this);

        btnLogiin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onSubmit();
            }
        });
    }

    public void onSubmit() {
        boolean isValid = presenter.validationLogin(etUsername.getText().toString(),
                etPassword.getText().toString());
        if(isValid){
                presenter.doLogin(etUsername.getText().toString(),etPassword.getText().toString());
        }

    }

    @Override
    public void onError(String Message) {
        Toast.makeText(this, Message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onSuccess(String Url) {
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(Url));
        startActivity(intent);
    }
}
